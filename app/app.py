from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    index_html = """
    <h1> Python Flask in Docker! </h1>
    <p> A sample web-app for running flask inside docker. </p>
    <a href="/hello/User"> Say Hello! </a>
    """
    return index_html


@app.route('/hello/<username>')
def hello(username):
    page_html = """
    <h1> Python Flask in Docker! </h1>
    <a href="/"> [Back to Homepage] </a>
    <hr>
    """
    page_html += "<h2> Hello, %s </h2>" % (username)
    page_html += "Modify URL to change greeting!"
    return page_html

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
